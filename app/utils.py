import os

from django.core.exceptions import ImproperlyConfigured

BOOL_MAP = {"true": True, "false": False}


def env(name, _type=str, required=False, default=None):
    value = os.getenv(name)

    if issubclass(_type, bool):
        try:
            value = BOOL_MAP[value.lower()]
        except KeyError:
            value = None
    elif issubclass(_type, int):
        try:
            value = int(value)
        except ValueError:
            value = None

    if required and value is None:
        raise ImproperlyConfigured(
            'Required environment variable "{}" is not set.'.format(name)
        )

    if value is None and default is not None:
        return default

    return value
