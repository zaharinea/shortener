from core.views import login
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from shortener.views import short_url_view

schema_view = get_schema_view(
    openapi.Info(
        title="Shortener API",
        default_version="v1",
        description="description",
        terms_of_service="",
        contact=openapi.Contact(email=""),
        license=openapi.License(name=""),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path("s/<str:code>", short_url_view),
    path("api/v1/login", login),
    path("api/v1/urls/", include("shortener.urls")),
    path("admin/", admin.site.urls),
    url(
        "api/v1/swagger(?P<format>\.json|\.yaml)$",  # NOQA W605
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    url(
        "api/v1/swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
]
