import pytest
from django.core.exceptions import ImproperlyConfigured

from app.utils import env


def test_env_required():
    with pytest.raises(ImproperlyConfigured):
        env(name="XXX", required=True)

    assert env(name="XXX", required=False) is None


def test_env_default():
    assert env(name="TEST", default="xxx") == "xxx"
    assert env(name="TEST", default=1) == 1
    assert env(name="TEST", default=True) is True


def test_env(monkeypatch):
    monkeypatch.setenv("ENV_STR", "xxx")
    assert env(name="ENV_STR") == "xxx"

    monkeypatch.setenv("ENV_INT", "1")
    assert env(name="ENV_INT", _type=int) == 1

    monkeypatch.setenv("ENV_BOOL", "true")
    assert env(name="ENV_BOOL", _type=bool) is True

    monkeypatch.setenv("ENV_BOOL", "True")
    assert env(name="ENV_BOOL", _type=bool) is True

    monkeypatch.setenv("ENV_BOOL", "false")
    assert env(name="ENV_BOOL", _type=bool) is False

    monkeypatch.setenv("ENV_BOOL", "False")
    assert env(name="ENV_BOOL", _type=bool) is False
