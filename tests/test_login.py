import pytest
from rest_framework.test import APIClient


@pytest.mark.django_db
def test_api_login(user):
    client = APIClient()
    response = client.post(
        f"/api/v1/login", data={"username": user.username, "password": "secret"}
    )

    assert response.status_code == 200
    assert response.data["token"] is not None
