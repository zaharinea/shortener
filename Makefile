clean:
	find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf

test:
	pytest -v --cov-report term --cov shortener

dep:
	pip install dev-requirements.txt

run:
	./manage.py runserver 0.0.0.0:8090

docker-run:
	docker-compose up
