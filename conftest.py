import pytest
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient


@pytest.fixture()
def user():
    User = get_user_model()
    return User.objects.create_user(
        username="user1", email="user1@example.com", password="secret", is_staff=True
    )


@pytest.fixture()
def auth_api_client_session(user) -> APIClient:
    client = APIClient()
    client.login(username=user.username, password="secret")
    return client


@pytest.fixture()
def auth_api_client_token(user) -> APIClient:
    client = APIClient()
    response = client.post(
        f"/api/v1/login", data={"username": user.username, "password": "secret"}
    )
    token = response.data["token"]
    client.credentials(HTTP_AUTHORIZATION="Token " + token)
    return client
