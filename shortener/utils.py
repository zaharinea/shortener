from datetime import datetime

from django.conf import settings
from django.utils.crypto import get_random_string

from . import consts, models


def generate_code() -> str:
    attempts = 5
    while attempts > 0:
        code = get_random_string(length=consts.CODE_LENGTH)
        if models.Url.objects.filter(code=code).exists():
            attempts -= 1
            continue
        return code
    raise ValueError("Failed gen unique code")


def get_short_url(url: models.Url) -> str:
    return f"{settings.API_HOSTNAME}/s/{url.code}"


def format_datetime(value: datetime) -> str:
    return value.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
