# Generated by Django 2.2 on 2019-04-15 09:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("shortener", "0001_initial")]

    operations = [
        migrations.AlterField(
            model_name="url",
            name="code",
            field=models.SlugField(max_length=16, unique=True),
        ),
        migrations.AlterField(
            model_name="url", name="full_url", field=models.URLField(max_length=4096)
        ),
    ]
