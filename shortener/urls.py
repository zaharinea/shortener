from django.urls import path

from . import views

urlpatterns = [
    path("", views.UrlListAPIView.as_view()),
    path("<int:pk>/", views.UrlDetailAPIView.as_view()),
]
