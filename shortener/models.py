from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django_lifecycle import LifecycleModelMixin, hook

from . import consts

User = get_user_model()


def valid_till():
    return timezone.now() + consts.DEFAULT_VALID_TILL


class Url(LifecycleModelMixin, models.Model):
    code = models.SlugField(
        max_length=consts.SHORT_MAX_LENGTH, unique=True, db_index=True
    )
    full_url = models.URLField(max_length=consts.FULL_MAX_LENGTH)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="urls")
    created_at = models.DateTimeField(default=timezone.now)
    valid_till = models.DateTimeField(default=valid_till)

    def is_valid(self):
        return timezone.now() < self.valid_till

    @property
    def max_valid_till(self):
        return self.created_at + consts.DEFAULT_VALID_TILL

    @hook("before_save", when="valid_till", is_not=None)
    @hook("before_update", when="valid_till", has_changed=True)
    def valid_till_change(self):
        if self.created_at > self.valid_till:
            raise ValueError("valid_till should be > created_at")

        # TODO fix me
        # if self.valid_till > self.max_valid_till:
        #     raise ValueError(f"valid_till should be < {self.max_valid_till}")
