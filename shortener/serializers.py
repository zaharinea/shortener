from rest_framework import serializers

from .models import Url
from .utils import get_short_url as get_url


class UrlSerializer(serializers.ModelSerializer):
    code = serializers.SlugField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    short_url = serializers.SerializerMethodField()

    def get_short_url(self, obj):
        return get_url(obj)

    class Meta:
        model = Url
        fields = ("id", "code", "short_url", "full_url", "valid_till", "created_at")
