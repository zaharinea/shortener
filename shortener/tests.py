import pytest
from django.conf import settings
from django.utils import timezone
from freezegun import freeze_time
from shortener import consts, models, utils


@pytest.mark.django_db
def test_crud_url(mocker, auth_api_client_token):
    mocker.patch("shortener.views.generate_code", return_value="XXX")
    now = timezone.now()
    with freeze_time(now):
        response = auth_api_client_token.post(
            f"/api/v1/urls/", data={"full_url": "http://vk.com"}, format="json"
        )

    expected_response = {
        "code": "XXX",
        "created_at": utils.format_datetime(now),
        "full_url": "http://vk.com",
        "id": 1,
        "short_url": f"{settings.API_HOSTNAME}/s/XXX",
        "valid_till": utils.format_datetime(now + consts.DEFAULT_VALID_TILL),
    }
    assert response.status_code == 201
    assert response.data == expected_response

    response = auth_api_client_token.get(f"/api/v1/urls/{expected_response['id']}/")
    assert response.status_code == 200
    assert response.data == expected_response

    response = auth_api_client_token.get(f"/api/v1/urls/")
    expected_list = {
        "count": 1,
        "pages": 1,
        "links": {"next": None, "previous": None},
        "data": [expected_response],
    }
    assert response.status_code == 200
    assert response.data == expected_list

    response = auth_api_client_token.delete(f"/api/v1/urls/{expected_response['id']}/")
    assert response.status_code == 204
    assert response.data is None

    assert models.Url.objects.filter(pk=expected_response["id"]).exists() is False
