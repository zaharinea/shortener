from datetime import timedelta

CODE_LENGTH = 6
SHORT_MAX_LENGTH = 16
FULL_MAX_LENGTH = 4096

DEFAULT_VALID_TILL = timedelta(days=365 * 5)

CACHE_TTL = 300
