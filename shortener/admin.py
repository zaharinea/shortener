from django.contrib import admin
from django.utils.html import format_html

from .models import Url
from .utils import get_short_url


@admin.register(Url)
class UrlAdmin(admin.ModelAdmin):
    list_display = ("__str__", "user", "short_url", "created_at", "valid_till")
    readonly_fields = ("created_at",)

    def short_url(self, obj):
        url = get_short_url(obj)
        return format_html(f'<a href="{url}">{url}</a>')
