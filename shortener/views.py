from django.core.cache import cache
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .consts import CACHE_TTL
from .filters import IsOwnerFilterBackend
from .models import Url
from .serializers import UrlSerializer
from .utils import generate_code


def short_url_view(request, code: str):
    full_url = cache.get(code)
    if full_url is not None:
        return redirect(full_url)

    url = get_object_or_404(Url, code=code)
    if not url.is_valid():
        raise Http404
    cache.set(code, url.full_url, timeout=CACHE_TTL)

    return redirect(url.full_url)


class UrlListAPIView(generics.ListCreateAPIView):
    queryset = Url.objects.all()
    serializer_class = UrlSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.validated_data["code"] = generate_code()
        serializer.validated_data["user_id"] = request.user.id

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


class UrlDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Url.objects.all()
    serializer_class = UrlSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
