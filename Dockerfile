FROM python:3.6-alpine

ENV PYTHONUNBUFFERED=1

RUN apk upgrade && \
    apk add --no-cache build-base gcc git linux-headers python3-dev openssl-dev libffi-dev postgresql-dev postgresql-client bash && \
    rm -rf /var/cache/apk/*

WORKDIR /app

RUN pip install uwsgi==2.0.17.1

COPY docker/app/docker-entrypoint.sh /app/
COPY docker/app/uwsgi.ini /app/
RUN chmod +x /app/docker-entrypoint.sh

COPY requirements.txt dev-requirements.txt ./
RUN pip install -r ./dev-requirements.txt

COPY . /app/

EXPOSE 8000

CMD ["/app/docker-entrypoint.sh"]
